A collection of python utility/convenience code
===============================================

Soupstone is a Python package containing a grab bag of convenience classes, functions, and
declarations. If I find I'm repeating myself and I don't have a better home, I'll put it here

TODO: Add more to description

* Include overview of the project
  * basic usage, examples

* Don't include changelog, but do a very brief "what's new" for this version
  and point to the changelog file

