

# setuptools quick guide

Soupstone is not yet on PyPi (and therefore not directly pip installable)

install to current site-packages in production mode:

```
python setup.py install
```


Install in development mode (code in place):

```
python setup.py develop

```

And to uninstall the development mode package:

```
python setup.py develop --uninstall
```

For more, check out the following links:
* <http://pythonhosted.org//setuptools/setuptools.html#development-mode>
* <http://pythonhosted.org/an_example_pypi_project/setuptools.html>

