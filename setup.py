#!/usr/bin/env python
#Convenience tool for working with bucket stores
#
# This setup script adapted from:
#   http://pythonhosted.org/setuptools/setuptools.html

from __future__ import print_function

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
#from setuptools.command.test import test as TestCommand
#import io

# To use a consistent encoding
import codecs
import os
import sys

import soupstone as package

here = os.path.abspath(os.path.dirname(__file__))

# Get the long description form the relevant file
with codecs.open(os.path.join(here, 'DESCRIPTION.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name=package.__title__,
    version=package.__version__,
    description=package.__summary__,
    long_description=long_description,
    url=package.__url__, 
    author=package.__author__,
    author_email=package.__author_email__,
    license=package.__license__,

    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),

    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=package.__package_dependencies__,

    # List additional groups of dependencies here (e.g. development
    # dependencies). You can install these using the following syntax,
    # for example:
    # $ pip install -e .[dev,test]
    #extras_require={
    #    'dev': ['check-manifest'],
    #    'test': ['coverage'],
    #},

    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    
    #package_data={
    #    package.__title__: ['package_data.dat'],
    #},

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files # noqa
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    
    #data_files=[('my_data', ['data/data_file'])],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    
    entry_points={
        'console_scripts': [
            'soupstone=soupstone:main',
        ],
        # 'gui_scripts': [
        #    'baz = my_package_gui:start_func',
        #],
    },


    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project?
        'Development Status :: 3 - Pre-Alpha',
        #'Development Status :: 3 - Alpha',
        #'Development Status :: 4 - Beta',
        
        'License :: OSI Approved :: BSD License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        #
        # Start out with this. Then add as we confirm through testing (setup tox)
        #

        #'Programming Language :: Python :: 2',
        #'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        #'Programming Language :: Python :: 3',
        #'Programming Language :: Python :: 3.2',
        #'Programming Language :: Python :: 3.3',
        #'Programming Language :: Python :: 3.4',        
        #'Programming Language :: Python :: 3.5',
        ],

        # TODO: Work on the keywords
        keywords='env os userspace database http storage',
    )

