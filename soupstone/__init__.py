# -*- coding: utf-8 -*-

"""
Soupstone grab bag of python utility and convenience classes and functions.

'it goes here when I find I'm repeating myself but haven't found a better home for the code'

Currently in soupstone:
    path - File system and web path helpers
    db - Database helpers


Copyright: (c) 2015 by John Baldwin.
License: BSD, see LICENSE for more details.

"""

__title__ = 'soupstone'
__version__ = '0.0.1'
__summary__ = 'grab bag of python utility and convenience classes, functions, and declarations'
__url__ = 'https://path/to/documentation'
#__build__ =
__author__ = 'John Baldwin'
__author_email__ = 'opensource@johnbaldwin.org'
__license__ = 'BSD License'
__copyrightt__ = 'Copyright 2015 John Baldwin'

__package_dependencies__ = [ ]

import argparse
import os

def main(argv=None):
    """This is a sample. If we really need a main script, we'll start here
    """
    print('This is a sample. Nothing really to do')
    # See: https://docs.python.org/3.4/library/os.html#os.EX_OK
    return os.EX_OK

