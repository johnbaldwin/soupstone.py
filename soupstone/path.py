import os

# Path helpers: Move to soupstone.paths
def abs_path(path):
    if not path:
        return None
    else:
        if path.startswith('~'):
            return os.path.expanduser(path)
        else:
            return os.path.abspath(path)

def user_home_path(username=None):
    """
    
    Arguments:
        username - the user login name/handle

    Exceptions:
        Raises ValueError exception if the directory is not found
    
    """
    user_home_path = os.path.expanduser('~{}'.format(username) if username else '~')
    if user_home_path.startswith('~'):
        raise ValueError('cannot find home path for username "{}".'.format(username))
    else:
        return user_home_path


